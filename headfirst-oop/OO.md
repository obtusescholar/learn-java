**What is INTERFACE?**
This code construct has the dual role of defining behaviour that applies to
multiple types, and also being the preferred focus of classes that use those
types.

**What is ENCAPSULATION?**
It's been responsible for preventing more maintenance problems than any other
OO principle in history, by localizing the changes required for the behaviour
of an object to vary.

**What is CHANGE?**
Every class should attempt to make sure that is has only one reason to do this,
the death of many a badly designed piece of software.


### Encapsulate what varies.

### Code to an interface rather than to an implementation.

### Each class in your application should have only one reason to change.
