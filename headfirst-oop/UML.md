# UML

| Java | UML | Marking
|---|---|---
| Abstract Class | Abstract Class | Italicized Class Name
| Relationship | Association | Arrow
| Inheritance | Generalization | Hollow Arrow
| Aggregation | Aggregation | Hollow Diamond

<details markdown='1'><summary>Expand / Collapse</summary>
testi
</details>