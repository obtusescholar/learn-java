import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class DogDoor {

    public boolean open;
    public List allowedBarks;

    // Remember constructor
    // Defines allowedBarks as LinkedList
    public DogDoor() {
        this.allowedBarks = new LinkedList();
        open = false;
    }

    public void open() {
        System.out.println("Dogdoor has opened");
        open = true;

        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                close();
                timer.cancel();
            }
        }, 5000);
    }

    public void close() {
        System.out.println("Dogdoor has closed");
        open = false;
    }

    public boolean isOpen() {
        return open;
    }

    public List getAllowedBarks() {
        return allowedBarks;
    }

    public void addAllowedBark(Bark bark) {
        allowedBarks.add(bark);
    }
}
