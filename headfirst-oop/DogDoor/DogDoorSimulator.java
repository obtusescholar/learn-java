public class DogDoorSimulator {
    
    public static void main(String[] args) {
        DogDoor door = new DogDoor();
        BarkRecognizer recognizer = new BarkRecognizer(door);
        Remote remote = new Remote(door);

        door.addAllowedBark(new Bark("Woof"));
        door.addAllowedBark(new Bark("Bark"));

        System.out.println("Dog starts barking");
        recognizer.recognize(new Bark("Woof"));

        System.out.println("Dog has gone outside");

        try {
            Thread.currentThread().sleep(7500);
        } catch (InterruptedException e) { }

        System.out.println("Dog has done it's business");

        System.out.println("Wrong dog starts barking");
        recognizer.recognize(new Bark("Rowlf"));

        try {
            Thread.currentThread().sleep(5000);
        } catch (InterruptedException e) { }

        System.out.println("Dog wants back inside");
        recognizer.recognize(new Bark("Woof"));

        System.out.println("Dog is back inside");
    }
}
