public class MyAnimalListTest {
    public static void main(String[] args) {
        MyAnimalList animalList = new MyAnimalList();

        Dog d = new Dog();
        Cat c = new Cat();

        animalList.add(d);
        animalList.add(c);

        Object a1 = animalList.get(0);
        Object a2 = animalList.get(1);

        if (a1 instanceof Dog) {
            Dog x = (Dog) a1;
            System.out.println(x);
        } else if (a1 instanceof Cat) {
            Cat x = (Cat) a1;
            System.out.println(x);
        }
    }
}
