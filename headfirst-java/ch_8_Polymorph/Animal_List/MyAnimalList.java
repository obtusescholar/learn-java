public class MyAnimalList {
    private Object[] objects = new Object[5];
    // private Animal[] animals = new Animal[5];
    // private Dog[] animals = new Dog[5];
    private int nextIndex = 0;

    public void add(Object o) {
        if (nextIndex < objects.length) {
            objects[nextIndex] = o;
            // animals[nextIndex] = o;
            System.out.println("Ojbect added at " + nextIndex);
            nextIndex++;
        }
    }

    public Object get(int i) {
        return objects[i];
        // return animals[i];
    }
}
