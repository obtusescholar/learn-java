import java.util.Scanner;


class SimpleDotComGame {
    public static void main(String[] args) {
        int numOfGuesses = 0;
        // GameHelper helper = new GameHelper();

        SimpleDotCom theDotCom = new SimpleDotCom();
        int randomNum = (int) (Math.random() * 5);

        int[] locations = {randomNum, randomNum + 1, randomNum + 2};
        theDotCom.setLocationCells(locations);
        boolean isAlive = true;

        while(isAlive == true) {
            // String guess = helper.getUserInput("enter a number");
            Scanner myObj = new Scanner(System.in);
            System.out.print("enter a number: ");
            String guess = myObj.nextLine();

            String result = theDotCom.checkYourSelf(guess);
            numOfGuesses++;
            if (result.equals("kill")) {
                isAlive = false;
                System.out.println("You took " + numOfGuesses + " guesses");
            }
        }
    }
}
