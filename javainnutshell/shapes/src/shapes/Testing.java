package shapes;

public class Testing {

	public static void main(String[] args) {
		Shape[] shapes = new Shape[3];
		shapes[0] = new Circle(2.0);
		shapes[1] = new Rectangle(1.0, 3.0);
		shapes[2] = new Rectangle(4.0, 2.0);
		
		double totalArea = 0;
		for(int i = 0; i < shapes.length; i++) {
			totalArea += shapes[i].area();
			System.out.println(totalArea);
		}
	}

}