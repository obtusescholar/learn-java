package overrideMethod;

public class TestPrint {

	public static void main(String[] args) {
		
		Car normal = new Car(120, 100, 4);
		SportsCar sport = new SportsCar(200);
		
		System.out.println("Normal car range: " + normal.range());
		System.out.println("Sports car range: " + sport.range());
	}

}
