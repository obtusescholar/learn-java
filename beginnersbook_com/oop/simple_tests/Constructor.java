public class Example2 {

    private int num;

    // Remember no type for constructors
    public Example2() {
        this.num = 2;
    }

    public Example2(int num) {
        this.num = num;
    }

    public int info() {
        return num;
    }

    public static void main(String[] args) {
        Example2 obj1 = new Example2();
        Example2 obj2 = new Example2(10);

        System.out.println("Value is: "+obj1.info());
        System.out.println("Value is: "+obj2.info());
    }
}
