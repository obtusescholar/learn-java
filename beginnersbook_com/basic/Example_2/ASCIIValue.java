public class ASCIIValue {
    public static void main(String[] args) {

        char ch = 'p';
        System.out.println("Char is "+ch);

        // sign character as int
        int signChar = ch;
        System.out.println(ch+" ASCII (sign char) value is "+signChar);

        // typecast character as int
        int typeChar = (int)ch;
        System.out.println(ch+" ASCII (typecast char)  value is "+typeChar);
    }
}
