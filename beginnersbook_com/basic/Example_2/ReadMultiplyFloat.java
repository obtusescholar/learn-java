import java.util.Scanner;

public class ReadMultiplyFloat {
    public static void main(String[] args) {

        // using double as data type for int and float numbers

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter first number: ");
        double num1 = scan.nextDouble();

        System.out.println("Enter second number: ");
        double num2 = scan.nextDouble();
        scan.close();

        double product = num1*num2;

        System.out.println("Numbers multiplied "+product);
    }
}
