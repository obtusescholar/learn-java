import java.util.Scanner;

class Triangle {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter width:");
        double width = scanner.nextDouble();

        System.out.println("Enter height");
        double height = scanner.nextDouble();
        scanner.close();

        double area = (width * height)/2;
        System.out.println("Area is "+area);
    }
}
