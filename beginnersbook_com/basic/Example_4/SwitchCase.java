public class SwitchCase {
    public static void main(String[] args) {

        // Will print Case 3 and Default
        int num = 3;

        // Will print Default
        // Passes through if no default set
        //num = 4;

        switch (num) {
            case 1:
                System.out.println("Case 1: Number is "+num);
            case 2:
                System.out.println("Case 2: Number is "+num);
            case 3:
                System.out.println("Case 3: Number is "+num);
                // With break will not print default also
                break;
            default:
                System.out.println("Default: Number is "+num);
        }
    }
}
