import java.util.Scanner;

public class TernaryCompare {
    public static void main(String[] args) {

        int num1, num2, num3, temp, result;

        Scanner sc = new Scanner(System.in);
        System.out.print("Give first number");
        num1 = sc.nextInt();

        System.out.print("Give second number");
        num2 = sc.nextInt();

        System.out.print("Give third number");
        num3 = sc.nextInt();
        sc.close();

        temp = num1>num2 ? num1:num2;
        result = temp>num3 ? temp:num3;

        System.out.println("Biggest number is "+result);
    }
}
