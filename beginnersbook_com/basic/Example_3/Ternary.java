public class Ternary {
    public static void main(String[] args) {

        int num1=20, num2;

        num2 = (num1 == 10) ? 100: 200;
        System.out.println("num2 is "+num2);

        num1 = 10;

        num2 = (num1 == 10) ? 100: 200;
        System.out.println("num2 is "+num2);
    }
}
