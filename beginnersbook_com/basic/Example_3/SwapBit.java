import java.util.Scanner;

public class SwapBit {
    public static void main(String[] args) {

        int num1, num2;

        Scanner sc = new Scanner(System.in);
        System.out.print("First number:");
        num1 = sc.nextInt();

        System.out.print("Second number:");
        num2 = sc.nextInt();
        sc.close();

        // System.out.println("First number "+num1);
        // System.out.println("Second number "+num2);

        System.out.println("XOR num1 and num2: "+num1+" ^ "+num2);
        num1 = num1 ^ num2;
        System.out.println(" num1 = "+num1);

        System.out.println("XOR num1 and num2: "+num1+" ^ "+num2);
        num2 = num2 ^ num1;
        System.out.println(" num2 = "+num2);

        System.out.println("XOR num1 and num2: "+num1+" ^ "+num2);
        num1 = num1 ^ num2;
        System.out.println(" num1 = "+num1);

        System.out.println("Swapped first number "+num1);
        System.out.println("Swapped second number "+num2);
    }
}
