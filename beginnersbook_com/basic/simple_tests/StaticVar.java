public class StaticVar {
    public static String myClassVar="class or static variable";

    public static void main(String args[]){
        StaticVar obj = new StaticVar();
        StaticVar obj2 = new StaticVar();
        StaticVar obj3 = new StaticVar();

        System.out.println(obj.myClassVar);
        System.out.println(obj2.myClassVar);
        System.out.println(obj3.myClassVar);
        System.out.println(myClassVar);

        obj.myClassVar = "Changed Text";

        System.out.println(obj.myClassVar);
        System.out.println(obj2.myClassVar);
        System.out.println(obj3.myClassVar);
        System.out.println(myClassVar);
    }
}
