public class EnhancedFor {
    public static void main(String[] args) {

        int arr[] = {1, 2, 3, 4};

        // Unpack array
        // Kinda pythonic...
        for(int value : arr) {

            // Remember to use the unpacked 'value' variable
            System.out.println("Array item: "+value);
        }
    }
}
