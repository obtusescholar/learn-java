class ByteExample {
    public static void main(String[] args) {
        byte num = 113;

        // byte value -128..127
        // byte num2 = 128;

        System.out.println(num);

        // compilation error
        // System.out.println(num2);
    }
}
