public class LocalVar {
    // make static variable
    public String myVar="instance variable";

    public void myMethod(){
        // cannot change the static variable
        String myVar = "Inside Method";
        String myVar2 = "Local Var 2";

        System.out.println(myVar);
        System.out.println(myVar2);
    }
    public static void main(String args[]){
        LocalVar obj = new LocalVar();

        System.out.println("Calling Method");
        // trying to change static variable with local variable assignment
        obj.myMethod();
        System.out.println(obj.myVar);

        // local variable cannot be called
        // System.out.println(obj.myVar2);
    }
}
