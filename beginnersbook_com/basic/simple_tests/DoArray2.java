public class DoArray2 {
    public static void main(String[] args) {

        String[] arr = {"one", "two", "three", "four", "five", "six", "seven"};
        int i = 0;

        do {
            System.out.println(arr[i]);
            i++;
        } while(i <= arr.length - 1);
    }
}
