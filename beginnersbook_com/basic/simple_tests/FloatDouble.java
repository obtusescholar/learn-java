public class FloatDouble {
    public static void main(String[] args) {

        double d = 22.4;

        // remember the 'f' at the end
        float f = 22.4f;

        System.out.println(d);
        System.out.println(f);

    }
}
