public class InstanceVar {
    String myInstanceVar="instance variable";

    public static void main(String args[]){
        InstanceVar obj = new InstanceVar();
        InstanceVar obj2 = new InstanceVar();
        InstanceVar obj3 = new InstanceVar();

        System.out.println(obj.myInstanceVar);
        System.out.println(obj2.myInstanceVar);
        System.out.println(obj3.myInstanceVar);

        // this does not work
        // System.out.println(myInstanceVar);

        obj.myInstanceVar = "changed text";

        System.out.println(obj.myInstanceVar);
        System.out.println(obj2.myInstanceVar);
        System.out.println(obj3.myInstanceVar);

    }
}
