import java.util.Scanner;

public class AskPositiveNegative {
    public static void main(String[] args) {

        // remember to close the scan
        int number;
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the number");
        number = scan.nextInt();
        scan.close();

        if(number > 0) {
            System.out.println(number+" is positive");
        }
        else if(number < 0) {
            System.out.println(number+" is negative");
        }
        // -0 is also neutral
        else {
            System.out.println(number+" is neutral");
        }
    }
}
