public class PositiveNegative {
    public static void main(String[] args) {
        int number=109;
        if(number > 0) {
            System.out.println(number+" is a positive");
        }
        else if(number < 0) {
            System.out.println(number+" is negative");
        }
        else {
            System.out.println(number+" is 'neutral'");
        }
    }
}
