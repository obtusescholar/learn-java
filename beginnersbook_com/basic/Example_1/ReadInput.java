// remember semicolor to the end
import java.util.Scanner;

public class ReadInput {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.print("Enter any number: ");

        int num = scan.nextInt();
        scan.close();

        System.out.println("The number is: "+num);
    }
}
